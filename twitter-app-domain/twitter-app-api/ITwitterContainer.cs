﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace twitter_app_api
{
    [ServiceContract]
    public interface ITwitterContainer
    {
        [OperationContract]
        String readTeets(string term);
    }
}
